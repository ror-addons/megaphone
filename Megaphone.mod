<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Megaphone" version="1.0" date="07/18/2009" >

		<Author name="Richard Conner" email="rkc@pacbell.net" />
		<Description text="v1.0 Makes your warband leaders instructions into system alerts" />

		<Dependencies>
			<Dependency name="EA_ChatWindow" />
		</Dependencies>
		
		<Files>
			<File name="Megaphone.lua" />
		</Files>
		
		<OnInitialize>
			<CallFunction name="Megaphone.Initialize" />
		</OnInitialize>
		
		<OnUpdate/>
		
		<OnShutdown/>
				
	</UiMod>
</ModuleFile>