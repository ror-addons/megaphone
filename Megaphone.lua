
Megaphone = {}

local playerFilter
local chatFilter = {}

function Megaphone.Initialize()
	--TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone v1.0 initializing.")
	
	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "Megaphone.Filter");
	RegisterEventHandler(SystemData.Events.GROUP_UPDATED, "Megaphone.GroupUpdate");
	RegisterEventHandler(SystemData.Events.GROUP_PLAYER_ADDED, "Megaphone.GroupPlayerAdd");
	
  --register slash commands
    if LibSlash then
        LibSlash.RegisterSlashCmd("megaphone", function(input) Megaphone.SlashHandler(input) end)
    end    
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone v1.0 ready.")
end

function Megaphone.SlashHandler(args)
  local opt, opt2, opt3, val, val2, val3
  local ttable, option
  opt, val = args:match("(%w+)[ ]?(.*)")

  if opt then
    playerFilter = Megaphone.prepPlayerName(opt)
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: set leader=[" .. playerFilter .. L"]")
  else
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"/megaphone playername");
  end
end

function Megaphone.GroupPlayerAdd()
    --TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: GroupPlayerAdd")
	Megaphone.findLeader();
end

function Megaphone.GroupUpdate()
	--TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: GroupUpdate")
	Megaphone.findLeader();
end


function Megaphone.findLeader()
	local leader;
	local player;
	local isGL;
	local lastKnownLeader = playerFilter;
	playerFilter = nil;
    local groups = GetBattlegroupMemberData();
    for group = 1, #groups do
      members = groups[group].players;
      --TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: group " .. group .. L" has " .. #members .. L" members")
      for member= 1, #members do
         player = members[member];
         if player.isInSameRegion and player.isGroupLeader then
           leader = player.name;
           playerFilter = Megaphone.prepPlayerName(leader)
           --TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: found leader [" .. player.name .. L"]")
           --break;
         else
           --TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: member [" .. player.name .. L"]")
         end
      end
    end
    if (playerFilter == nil) then
        playerFilter=lastKnownLeader;
    end
    if (playerFilter ~= lastKnownLeader) then
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: found new leader=[" .. playerFilter .. L"]")
    end
end

function Megaphone.Filter()

	local chatType = GameData.ChatData.type
	local chatSender = Megaphone.prepPlayerName(GameData.ChatData.name)
	local chatText = GameData.ChatData.text:lower()
	
    if (string.find(WStringToString(chatText), "added to the warband",1,1)) then
	    --TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L"Megaphone: chat added to warband")
    	Megaphone.findLeader();
	    return;
	end
	
	if chatSender == playerFilter then 
    	if chatType == SystemData.ChatLogFilters.GROUP or
	      chatType == SystemData.ChatLogFilters.BATTLEGROUP or
		  --chatType == SystemData.ChatLogFilters.SHOUT or
		  --chatType == SystemData.ChatLogFilters.SAY or
		  chatType == SystemData.ChatLogFilters.SCENARIO_GROUPS then
		    -- Do Alert Message here...
		    PlaySound(GameData.Sound.QUEST_COMPLETED);
		    AlertTextWindow.AddLine(SystemData.AlertText.Types.MOVEMENT_RVR, chatSender .. L": " .. chatText);
			--TextLogAddEntry("Chat", SystemData.ChatLogFilters.TELL_RECEIVE, L" MEGAPHONE: '" .. playerFilter .. L"' " .. chatText)
		  end
	end

end

function Megaphone.prepPlayerName(name)
	-- Inspired by a function by Skoli, author of SKNotice.
	local playerName = name:lower()
	if(type(playerName) == "wstring") then
		playerName =  WStringToString(playerName):match("([^^]+)^?([^^]*)")
	elseif(type(playerName) == "string") then
		playerName = playerName:match("([^^]+)^?([^^]*)")
	end
	playerName = towstring(playerName)
	return playerName
end
